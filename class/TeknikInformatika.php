<?php

require 'ProgramStudi.php';

class TeknikInformatika extends ProgramStudi {
    function setNama($nama){
        $this->nama = $nama;
    }

    function getNama() : string {
        return $this->nama;
    }
}