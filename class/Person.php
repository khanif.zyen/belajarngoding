<?php

//mendefinisikan class
//class adalah sebuah blueprint/cetakan
//class terdiri dari property dan method.
class Person {
    //property
    //visibility modifier: 
        //public: bisa diakses oleh class dan object/instance 
        //protected: bisa diakses hanya class dan turunan class
        //private: hanya diakses oleh class itu sendiri
    public $name; 
    public $alamat;
    protected $tanggal_lahir;
    public $umur;

    //constructor: fungsi yang pertama kali dijalankan ketika inisiasi object
    function __construct($name,$alamat){
        $this->name = $name;
        $this->alamat = $alamat;
    }

    //method
    function setName($name){
        $this->name = $name;
    }

    function setAlamat($alamat){
        $this->alamat = $alamat;
    }

    function setTanggalLahir($tanggal_lahir){
        $this->tanggal_lahir = $tanggal_lahir;
    }

    function setUmur($umur){
        $this->umur = $umur;
    }

    function getName(){
        return $this->name;
    }

    function getAlamat(){
        return $this->alamat;
    }

    function getUmur(){
        return $this->umur;
    }

    function getTanggalLahir(){
        return $this->tanggal_lahir;
    }

}
