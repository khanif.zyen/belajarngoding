<?php

require 'Person.php';

class Mahasiswa extends Person { //1.inheritance
    public $nim;

    const UNIV = "Universitas Islam Nahdlatul Ulama"; //constant

    public static $makul = "Belajar  Ngoding"; //static property

    function __construct($nim){
        $this->nim = $nim;
    }

    static function getFakultas(){ //static method
        return "SAINTEK";
    }
}