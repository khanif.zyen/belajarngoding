<?php

abstract class ProgramStudi {
    public $nama;

    abstract function setNama($nama);
    abstract function getNama();
}