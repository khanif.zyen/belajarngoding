<?php

//require 'class/Person.php';
require 'class/Mahasiswa.php';
require 'class/TeknikInformatika.php';

//yang digunakan adalah hasil cetakan/instance/object 
//mendefinisikan object
$adi = new Person('Adi','Kawak');
//$adi->setName('Adi');
//$adi->name = 'Adi';
//echo $adi->name;
//$adi->setAlamat('Kawak');
$adi->setTanggalLahir('1 Juni 1999');
//$adi->tanggal_lahir = '1 Juni 1999'; //error
$adi->setUmur(20);
echo $adi->getName()."<br/>";
echo $adi->getAlamat()."<br/>";
echo $adi->getTanggalLahir()."<br/>";
echo $adi->getUmur()."<br/>";

$aji = new Person('Aji','Ngabul');
//$aji->setName('Aji');
//$aji->setAlamat('Ngabul');
$aji->setTanggalLahir('13 Juni 1999');
$aji->setUmur(20);
echo $aji->getName()."<br/>";
echo $aji->getAlamat()."<br/>";
echo $aji->getTanggalLahir()."<br/>";
echo $aji->getUmur()."<br/>";

$saifur = new Mahasiswa('171240000662');
echo $saifur->nim."<br/>";
$saifur->setName('Saifur');
$saifur->setAlamat('Kalinyamatan');
$saifur->setTanggalLahir('5 Maret 1999');
$saifur->setUmur(20);
echo $saifur->getName()."<br/>";
echo $saifur->getAlamat()."<br/>";
echo $saifur->getTanggalLahir()."<br/>";
echo $saifur->getUmur()."<br/>";
echo Mahasiswa::UNIV."<br/>";
echo Mahasiswa::getFakultas()."<br/>";
echo Mahasiswa::$makul."<br/>";

$tif = new TeknikInformatika();
$tif->setNama('Teknik Informatika Unisnu');
echo $tif->getNama();




